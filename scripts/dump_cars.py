from django.db.models import F

from vacancies.models import (
    Brand, Model, Gallery, EuroNcap, Package,
    Motor, Electric, Hybrid, Fuel, Control, Transmission, Suspension, Dimension, OffRoad,
    Still, Comfort, Security, Paket, Option,
    Salon
)
import json


def save_json(name, dictionary):
    json_object = json.dumps(dictionary, indent=4)
    with open(name, "w") as outfile:
        outfile.write(json_object)


def run():
    result_cars = {}
    result_cars['models'] = list(Model.objects.all().annotate(brand_name=F('brand__name')).distinct().values('id', 'brand_name', 'name', 'type', 'body', 'years', 'kms', 'link', 'comment'))
    result_cars['gallery'] = list(Gallery.objects.all().annotate(model_name=F('model__name'), brand_name=F('model__brand__name')).distinct().values())
    result_cars['euroncap'] = list(EuroNcap.objects.all().annotate(model_name=F('model__name'), brand_name=F('model__brand__name')).distinct().values())
    result_cars['package'] = list(Package.objects.all().annotate(model_name=F('model__name'), brand_name=F('model__brand__name')).distinct().values('brand_name', 'id', 'model_name', 'name', 'fiyat', 'order', 'fuel_index', 'fuel_type', 'transmiss', 'year', 'sites', 'comment'))

    result_cars['Motor'] = list(Motor.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Electric'] = list(Electric.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Hybrid'] = list(Hybrid.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Fuel'] = list(Fuel.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Control'] = list(Control.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Transmission'] = list(Transmission.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Suspension'] = list(Suspension.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['OffRoad'] = list(OffRoad.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Dimension'] = list(Dimension.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Paket'] = list(Paket.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Option'] = list(Option.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())

    result_cars['Still'] = list(Still.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Comfort'] = list(Comfort.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    result_cars['Security'] = list(Security.objects.all().annotate(model_name=F('package__model__name'), package_name=F('package__name'), brand_name=F('package__model__brand__name'), package_fiyat=F('package__fiyat'), package_fuel_type=F('package__fuel_type'), package_transmiss=F('package__transmiss'), package_fuel_index=F('package__fuel_index'), package_sites=F('package__sites')).distinct().values())
    save_json('dump_cars.json', result_cars)

    result_salons = {}
    result_salons['salons'] = list(Salon.objects.all().annotate(brand_name=F('brand__name'), city_name=F('city__name')).distinct().values(
        'brand_name', 'city_name', 'id', 'name', 'address', 'google', 'lat', 'lng', 'site', 'phone_number',
        'city_number', 'email', 'email_captain', 'email_sale', 'email_marketing', 'avatar', 'site_salon', 'email_salon',
    ))
    save_json('dump_salons.json', result_salons)

