# Python
import json
import requests
import traceback


def send_message_to_user(chat_id, text, reply_markup=None):
    token = '2035603902:AAGP0XChroTvHW4sE2oSDMOgwJUOiRG5elw'
    url = 'https://api.telegram.org/bot{}/sendMessage'.format(token)
    try:
        payload = {
            'chat_id': chat_id,
            'text': text,
            'parse_mode': 'HTML',
            'reply_markup': json.dumps(reply_markup) if reply_markup else {}
        }
        r = requests.post(url, data=payload)
        # print(r.content)
    except:
        print(traceback.print_exc())

#
# def send_invitations(chat_id, department, position, country):
#     groups = {
#         "Глобал": [
#             ["TDS Media", "-1001309969969"],
#             ["TDS-канал: Инновации, факапы и лайфхаки", "https://t.me/joinchat/q6otLG4dh_g3NzAy"],
#             ["TDS Канал - хорошие новости и благодарности", "https://t.me/joinchat/zNZHDi6vSVszZTNi"]
#         ],
#         "Руководство": [
#             ["КАМы_КомДир_Дир", "-1001457798091"],
#             ["Задачи для всей ТК", "https://t.me/joinchat/_FUqRPd3ZHE3NmQy"],
#
#         ],
#         "Продажи": [
#             ["Задачи для всей ТК", "https://t.me/joinchat/_FUqRPd3ZHE3NmQy"],
#             ["Тендер", "-1001179470008"],
#             ["Скрины рекламы", "-415854699"],
#             ["Обучение 11В", "-487898764"],
#             ["Пополнение", "https://t.me/joinchat/sCvSqn03-CQ0M2Fi"],
#             ["Автоматизация 2021", "-1001257530795"],
#             ["Запуск рекламных кампаний", "-1001563702132"]
#         ],
#         "Контекст": [
#             ["Запуск рекламных кампаний", "-1001563702132"]
#         ],
#         "Финансы": [
#         ],
#         "Управление персоналом": [
#         ],
#         "IT": [
#             ["Автоматизация 2021", "-1001257530795"]
#         ],
#         "Монетизация": [
#             ["TDS Монетизация", "-1001490685367"]
#         ],
#         "Коммерческий директор": [
#             ["КАМы_КомДир_Дир", "-1001457798091"],
#         ],
#         "Коммерческий директор (RU)": [
#             ["КАМы_КомДир_Дир", "-1001457798091"],
#         ],
#         "Key Account Manager": [
#             ["КАМы_КомДир_Дир", "-1001457798091"],
#         ]
#     }
#
#     groups_rus = {
#         "Общие": [
#             ["TDS.Moscow", "-1001373452333"]
#         ]
#     }
#     # Глобальные группы
#     text = "Присоединитесь ко всем группам и каналам по ссылкам, приведенным ниже: \n"
#     for group in groups["Глобал"]:
#         if len(group[1]) < 15:
#             # print(group[0])
#             invite = create_invite_for_group(group[1])
#             if invite['ok']:
#                 text = text + "[" + group[0] + "](" + invite["result"] + ")\n"
#             else:
#                 text = text + group[0] + ": не удалось создать ссылку\n"
#         else:
#             text = text + "[" + group[0] + "](" + group[1] + ")\n"
#
#     # Для России
#     try:
#         if country == "Россия":
#             for group in groups_rus["Общие"]:
#                 if len(group[1]) < 15:
#                     # print(group[0])
#                     invite = create_invite_for_group(group[1])
#                     if invite['ok']:
#                         text = text + "[" + group[0] + "](" + invite["result"] + ")\n"
#                     else:
#                         text = text + group[0] + ": не удалось создать ссылку\n"
#                 else:
#                     text = text + "[" + group[0] + "](" + group[1] + ")\n"
#         # Для КЗ и Глобал
#         if department in groups:
#             for group in groups[department]:
#                 if len(group[1]) < 15:
#                     # print(group[0])
#                     invite = create_invite_for_group(group[1])
#                     if invite['ok']:
#                         text = text + "[" + group[0] + "](" + invite["result"] + ")\n"
#                     else:
#                         text = text + group[0] + ": не удалось создать ссылку\n"
#                 else:
#                     text = text + "[" + group[0] + "](" + group[1] + ")\n"
#         if position in groups:
#             for group in groups[position]:
#                 if len(group[1]) < 15:
#                     # print(group[0])
#                     invite = create_invite_for_group(group[1])
#                     if invite['ok']:
#                         text = text + "[" + group[0] + "](" + invite["result"] + ")\n"
#                     else:
#                         text = text + group[0] + ": не удалось создать ссылку\n"
#                 else:
#                     text = text + "[" + group[0] + "](" + group[1] + ")\n"
#
#     except Exception as e:
#         print(e)
#         return "{}".format(e)
#     send_web_pages_to_user(chat_id, text)
#     return ""
#
#
# def send_web_pages_to_user(chat_id, text):
#     token = '2035603902:AAGP0XChroTvHW4sE2oSDMOgwJUOiRG5elw'
#
#     try:
#         r = requests.get(
#             'https://api.telegram.org/bot{}/sendMessage?chat_id={}&text={}&parse_mode=Markdown&disable_web_page_preview=True'.format(
#                 token,
#                 chat_id,
#                 text
#             )
#         )
#     except Exception as e:
#         print(e)
#
#
# def create_invite_for_group(chat_id):
#     token = '2035603902:AAGP0XChroTvHW4sE2oSDMOgwJUOiRG5elw'
#     try:
#         r = requests.get(
#             'https://api.telegram.org/bot{}/exportChatInviteLink?chat_id={}'.format(
#                 token,
#                 chat_id,
#             )
#         )
#         print(r.json())
#         return r.json()
#     except Exception as e:
#         print(e)
#         return {"ok": False, "error": "{}".format(e)}
