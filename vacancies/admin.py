from django.contrib import admin
from vacancies.models import *


class BrandAdmin(admin.ModelAdmin):
    model = Brand
    list_display = ('name', 'order')
    search_fields = ('name', )


class CityAdmin(admin.ModelAdmin):
    model = City
    list_display = ('id', 'name',)
    search_fields = ('name',)


class GalleryInline(admin.TabularInline):
    model = Gallery
    extra = 0


class EuroNcapInline(admin.TabularInline):
    model = EuroNcap
    extra = 0


class ModelAdmin(admin.ModelAdmin):
    model = Model
    list_display = ('name', 'brand', 'body', 'years', 'kms')
    search_fields = ('name', 'brand',)
    list_filter = ('body',)
    readonly_fields = (
        'created_at',
        'created_by',
    )
    inlines = [GalleryInline, EuroNcapInline]

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related('brand')

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super().save_model(request, obj, form, change)


class MotorInline(admin.TabularInline):
    model = Motor
    extra = 0


class FuelInline(admin.TabularInline):
    model = Fuel
    extra = 0


class ControlInline(admin.TabularInline):
    model = Control
    extra = 0


class TransmissionInline(admin.TabularInline):
    model = Transmission
    extra = 0


class SuspensionInline(admin.TabularInline):
    model = Suspension
    extra = 0


class HybridInline(admin.TabularInline):
    model = Hybrid
    extra = 0


class ElectricInline(admin.TabularInline):
    model = Electric
    extra = 0


class DimensionInline(admin.TabularInline):
    model = Dimension
    extra = 0


class OffRoadInline(admin.TabularInline):
    model = OffRoad
    extra = 0


class StillInline(admin.TabularInline):
    model = Still
    extra = 0


class ComfortInline(admin.TabularInline):
    model = Comfort
    extra = 0


class SecurityInline(admin.TabularInline):
    model = Security
    extra = 0


class PaketInline(admin.TabularInline):
    model = Paket
    extra = 0


class OptionInline(admin.TabularInline):
    model = Option
    extra = 0


class PackageAdmin(admin.ModelAdmin):
    model = Package
    list_display = (
        'model',
        'name',
        'fiyat',
        'order',
        'fuel_index',
        'fuel_type',
        'transmiss',
        'year',
    )
    search_fields = ('name',)
    list_filter = (
        'fuel_type',
        'transmiss',
        'year'
    )
    inlines = [
        MotorInline,
        ElectricInline,
        HybridInline,
        FuelInline,
        ComfortInline,
        TransmissionInline,
        SuspensionInline,
        DimensionInline,
        OffRoadInline,
        StillInline,
        ComfortInline,
        SecurityInline,
        PaketInline,
        OptionInline,
    ]


class SalonAdmin(admin.ModelAdmin):
    model = Salon
    list_filter = (
        'city',
        'brand',
    )
    list_display = (
        'id',
        'get_brands',
        'city',
        'name',
        'address',
        'phone_number',
        'city_number',
        'email',
        'site',
        'site_salon',
        'email_salon',
        'google',
        'avatar',
        'created_at',
        'created_by',
    )
    readonly_fields = (
        'created_at',
        'created_by',
    )
    search_fields = ('name',)

    def get_queryset(self, request):
        qs = super().get_queryset(request)
        return qs.prefetch_related('brand')

    def get_brands(self, obj):
        return "\n".join([brand.name for brand in obj.brand.all()])

    def save_model(self, request, obj, form, change):
        obj.created_by = request.user
        super().save_model(request, obj, form, change)


admin.site.register(City, CityAdmin)
admin.site.register(Salon, SalonAdmin)
admin.site.register(Brand, BrandAdmin)
admin.site.register(Model, ModelAdmin)
admin.site.register(Package, PackageAdmin)
