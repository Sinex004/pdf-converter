from django.contrib.auth.models import User, AnonymousUser
from django.db.models import Avg
from rest_framework import serializers

from vacancies.models import (
    Brand, Model, Gallery, EuroNcap, Package, Motor, Fuel, Control, Transmission,
    Suspension, Dimension, OffRoad, Still, Comfort, Security, Paket, Option, City, Hybrid, Electric, Salon, Comment,
    Rating
)


class CitySerializer(serializers.ModelSerializer):
    salon_count = serializers.SerializerMethodField('get_salon_count', read_only=True)

    def get_salon_count(self, instance):
        salon_count = instance.salon_set.all().count()
        return salon_count

    class Meta:
        model = City
        fields = [
            'name',
            'name_eng',
            'salon_count'
        ]


class BrandSerializer(serializers.ModelSerializer):
    class Meta:
        model = Brand
        fields = '__all__'


class OptionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Option
        fields = '__all__'


class PaketSerializer(serializers.ModelSerializer):
    additional = serializers.SerializerMethodField('get_options', read_only=True)

    def get_options(self, instance):
        res = instance.additional
        if res:
            return res.values()
        else:
            return []

    class Meta:
        model = Paket
        fields = [
            'name',
            'cost',
            'additional',
        ]


class PaketSerializer1(serializers.ModelSerializer):

    class Meta:
        model = Paket
        fields = [
            'name',
            'cost',
            'additional',
        ]


class SecuritySerializer(serializers.ModelSerializer):
    class Meta:
        model = Security
        fields = '__all__'


class ComfortSerializer(serializers.ModelSerializer):
    class Meta:
        model = Comfort
        fields = '__all__'


class StillSerializer(serializers.ModelSerializer):
    class Meta:
        model = Still
        fields = '__all__'


class OffRoadSerializer(serializers.ModelSerializer):
    class Meta:
        model = OffRoad
        fields = '__all__'


class DimensionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Dimension
        fields = '__all__'


class SuspensionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Suspension
        fields = '__all__'


class TransmissionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transmission
        fields = '__all__'


class ControlSerializer(serializers.ModelSerializer):
    class Meta:
        model = Control
        fields = '__all__'


class FuelSerializer(serializers.ModelSerializer):
    class Meta:
        model = Fuel
        fields = '__all__'


class ElectricSerializer(serializers.ModelSerializer):
    class Meta:
        model = Electric
        fields = '__all__'


class HybridSerializer(serializers.ModelSerializer):
    class Meta:
        model = Hybrid
        fields = '__all__'


class MotorSerializer(serializers.ModelSerializer):
    class Meta:
        model = Motor
        fields = '__all__'


class EuroNcapSerializer(serializers.ModelSerializer):
    class Meta:
        model = EuroNcap
        fields = '__all__'


class GallerySerializer(serializers.ModelSerializer):
    class Meta:
        model = Gallery
        fields = '__all__'


class PackageSerializer(serializers.ModelSerializer):
    option_set = OptionSerializer(read_only=True, many=True)
    paket_set = PaketSerializer(read_only=True, many=True)
    security_set = SecuritySerializer(read_only=True, many=True)
    comfort_set = ComfortSerializer(read_only=True, many=True)
    still_set = StillSerializer(read_only=True, many=True)
    offroad_set = OffRoadSerializer(read_only=True, many=True)
    dimension_set = DimensionSerializer(read_only=True, many=True)
    suspension_set = SuspensionSerializer(read_only=True, many=True)
    transmission_set = TransmissionSerializer(read_only=True, many=True)
    control_set = ControlSerializer(read_only=True, many=True)
    fuel_set = FuelSerializer(read_only=True, many=True)
    motor_set = MotorSerializer(read_only=True, many=True)
    hybrid_set = HybridSerializer(read_only=True, many=True)
    electric_set = ElectricSerializer(read_only=True, many=True)

    class Meta:
        model = Package
        fields = '__all__'


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'


class ModelSerializer(serializers.ModelSerializer):
    package_set = PackageSerializer(read_only=True, many=True)
    euroncap_set = EuroNcapSerializer(read_only=True, many=True)
    gallery_set = GallerySerializer(read_only=True, many=True)
    created_by = UserSerializer(read_only=True)

    class Meta:
        model = Model
        fields = '__all__'

# ############################################################################################################


class ModelConfigSerializer(serializers.ModelSerializer):
    package_count = serializers.SerializerMethodField('get_package_count', read_only=True)

    def get_package_count(self, instance):
        package_count = instance.package_set.all().count()
        return package_count

    class Meta:
        model = Model
        fields = [
            'name',
            'package_count',
        ]


class BrandConfigSerializer(serializers.ModelSerializer):
    model_set = ModelConfigSerializer(read_only=True, many=True)
    salon_count = serializers.SerializerMethodField('get_salon_count', read_only=True)

    def get_salon_count(self, instance):
        salon_count = instance.salon_set.all().count()
        return salon_count

    class Meta:
        model = Brand
        fields = [
            'name',
            'model_set',
            'salon_count',
        ]


class ModelCardSerializer(serializers.ModelSerializer):
    brand = BrandSerializer(read_only=True)
    fiyat_min = serializers.SerializerMethodField('get_fiyat_min', read_only=True)
    fiyats = serializers.SerializerMethodField('get_fiyats', read_only=True)
    rating_avg = serializers.SerializerMethodField('get_rating_avg', read_only=True)
    gallery_set = GallerySerializer(read_only=True, many=True)
    # avatar = serializers.SerializerMethodField('get_avatar', read_only=True)
    #
    # def get_avatar(self, instance):
    #     avatar = instance.gallery_set.filter(is_first='Да').first()
    #     if avatar:
    #         return avatar
    #     else:
    #         return ''

    def get_fiyat_min(self, instance):
        fiyat_min_package = instance.package_set.all().order_by('fiyat').first()
        if fiyat_min_package:
            return fiyat_min_package.fiyat
        else:
            return 0

    def get_fiyats(self, instance):
        fiyats = instance.package_set.all().order_by('fiyat').values_list('fiyat', flat=True)
        if fiyats:
            return fiyats
        else:
            return [0]

    def get_rating_avg(self, instance):
        rating = instance.rating_set.all().aggregate(avg_rating=Avg('score'))['avg_rating']
        if rating:
            return rating
        else:
            return 0

    class Meta:
        model = Model
        fields = [
            'id',
            'brand',
            'name',
            'fiyat_min',
            'rating_avg',
            'gallery_set',
            'fiyats'
        ]


class PackageOpenSerializer(serializers.ModelSerializer):
    option_set = OptionSerializer(read_only=True, many=True)
    paket_set = PaketSerializer(read_only=True, many=True)
    hybrid_set = HybridSerializer(read_only=True, many=True)
    electric_set = ElectricSerializer(read_only=True, many=True)
    group_1 = serializers.SerializerMethodField('get_group_1', read_only=True)
    group_2 = serializers.SerializerMethodField('get_group_2', read_only=True)
    group_3 = serializers.SerializerMethodField('get_group_3', read_only=True)
    group_4 = serializers.SerializerMethodField('get_group_4', read_only=True)
    group_5 = serializers.SerializerMethodField('get_group_5', read_only=True)
    group_6 = serializers.SerializerMethodField('get_group_6', read_only=True)

    def get_group_1(self, instance):
        res = list(instance.motor_set.all().values_list(
            'capacity_l',  # 0
            'capacity_cc',  # 1
            'power_hp',  # 2
            'power_rpm',  # 3
            'power_kw',  # 4
            'torque',  # 5
            'torque_min',  # 6
            'torque_max',  # 7
            'cylinder',  # 8
            'valve',  # 9
            'compression',  # 10
            'piston',  # 11
            'fuel_system',  # 12
            )[0])
        res.extend(instance.transmission_set.all().values_list(
            'transmission',  # 13
            'clutch',  # 14
            'gears',  # 15
            'drive',  # 16
        )[0])
        res.extend(instance.suspension_set.all().values_list(
            'brake_front',  # 17
            'brake_back',  # 18
            'front',  # 19
            'back',   # 20
            'front_damper',  # 21
            'back_damper',  # 22
        )[0])
        res.extend(instance.control_set.all().values_list(
            'acceleration',  # 23
            'speed',  # 24
            'turn_radius',  # 25
            'wheel_turns',  # 26
        )[0])
        res.extend(instance.offroad_set.all().values_list(
            'clearance',  # 27
            'angle_front',  # 28
            'angle_back',  # 29
            'angle_ramp',  # 30
            'depth',  # 31
        )[0])
        return res

    def get_group_2(self, instance):
        res = instance.fuel_set.all().values_list(
            'consumption',  # 0
            'consumption_city',  # 1
            'consumption_road',  # 2
            'tank',  # 3
            'co2_min',  # 4
            'co2_max',  # 5
            'emission_standard',  # 6
            )[0]
        return res

    def get_group_3(self, instance):
        res = instance.dimension_set.all().values_list(
            'length',  # 0
            'width',  # 1
            'height',  # 2
            'wheel_base',  # 3
            'width_front',  # 4
            'width_back',  # 5
            'overhang_front',  # 6
            'overhang_back',  # 7
            'trunk',  # 8
            'trunk_saloon',  # 9
            'trunk_second',  # 10
            'weight_driver',  # 11
            'weight_pass',  # 12
            'trailer',  # 13
            'trailer_brakes',  # 14
            'tires_front',  # 15
            'tires_back',  # 16
            )[0]
        return res

    def get_group_4(self, instance):
        res = instance.still_set.all().first().additional
        if res:
            return res.values()
        else:
            return []

    def get_group_5(self, instance):
        res = instance.comfort_set.all().first().additional
        if res:
            return res.values()
        else:
            return []

    def get_group_6(self, instance):
        res = instance.security_set.all().first().additional
        if res:
            return res.values()
        else:
            return []

    class Meta:
        model = Package
        fields = '__all__'


class SubCommentSerializer(serializers.ModelSerializer):
    rating = serializers.SerializerMethodField('get_rating', read_only=True)

    def get_rating(self, instance):
        rating = Rating.objects.filter(user=instance.user, model=instance.model).first()
        return rating.score

    class Meta:
        model = Comment
        fields = [
            'user',
            'message',
            'likes',
            'dislikes',
            'created_at',
            'rating',
        ]


class CommentSerializer(serializers.ModelSerializer):
    answers = SubCommentSerializer(read_only=True, many=True)
    user = UserSerializer(read_only=True)
    rating = serializers.SerializerMethodField('get_rating', read_only=True)
    likes_count = serializers.SerializerMethodField('get_likes_count', read_only=True)
    dislikes_count = serializers.SerializerMethodField('get_dislikes_count', read_only=True)

    def get_rating(self, instance):
        rating = Rating.objects.filter(user=instance.user, model=instance.model).first()
        if rating:
            return rating.score
        else:
            return None

    def get_likes_count(self, instance):
        return instance.likes.count()

    def get_dislikes_count(self, instance):
        return instance.dislikes.count()

    class Meta:
        model = Comment
        fields = [
            'id',
            'user',
            'message',
            'likes_count',
            'dislikes_count',
            'created_at',
            'answers',
            'rating',
        ]


class ModelOpenSerializer(serializers.ModelSerializer):
    package_set = PackageOpenSerializer(read_only=True, many=True)
    comments_top = serializers.SerializerMethodField('get_comments_top', read_only=True)
    euroncap_set = EuroNcapSerializer(read_only=True, many=True)
    gallery_set = GallerySerializer(read_only=True, many=True)
    brand_name = serializers.SerializerMethodField('get_brand_name', read_only=True)
    salons = serializers.SerializerMethodField('get_salons', read_only=True)
    rating = serializers.SerializerMethodField('get_rating', read_only=True)
    rating_avg = serializers.SerializerMethodField('get_rating_avg', read_only=True)

    def get_brand_name(self, instance):
        return instance.brand.name

    def get_salons(self, instance):
        city = self.context.get("city")
        return instance.brand.salon_set.filter(city=city).distinct().values(
            'id', 'name', 'avatar'
        )

    def get_comments_top(self, instance):
        comments_top = instance.comments.all().order_by('-operations_count')[:3]
        return CommentSerializer(comments_top, many=True).data

    def get_rating(self, instance):
        user = self.context.get("user")
        if isinstance(user, AnonymousUser):
            return None
        rating = instance.rating_set.filter(user=user).first()
        if rating:
            return rating.score
        else:
            return None

    def get_rating_avg(self, instance):
        rating = instance.rating_set.all().aggregate(avg_rating=Avg('score'))['avg_rating']
        if rating:
            return rating
        else:
            return 0

    class Meta:
        model = Model
        fields = '__all__'


class ModelGallerySerializer(serializers.ModelSerializer):
    gallery_set = GallerySerializer(read_only=True, many=True)
    info = serializers.SerializerMethodField('get_info', read_only=True)
    brand_name = serializers.SerializerMethodField('get_brand_name', read_only=True)
    rating_avg = serializers.SerializerMethodField('get_rating_avg', read_only=True)

    def get_rating_avg(self, instance):
        rating = instance.rating_set.all().aggregate(avg_rating=Avg('score'))['avg_rating']
        if rating:
            return rating
        else:
            return 0

    def get_brand_name(self, instance):
        return instance.brand.name
    # avatar = serializers.SerializerMethodField('get_avatar', read_only=True)
    #
    # def get_avatar(self, instance):
    #     avatar = instance.gallery_set.filter(is_first='Да').first()
    #     if avatar:
    #         return avatar.image
    #     else:
    #         return ''

    def get_info(self, instance):
        packages = instance.package_set.all().order_by('fiyat')
        cities = instance.brand.salon_set.all().values('city__name', 'city__name_eng').distinct()
        fiyat_min_package = packages.first()
        fiyat_max_package = packages.last()
        data = {
            'package_count': packages.count(),
            'fiyat_min': fiyat_min_package.fiyat if fiyat_min_package else 0,
            'fiyat_max': fiyat_max_package.fiyat if fiyat_max_package else 0,
            'cities': cities
        }
        return data

    class Meta:
        model = Model
        fields = '__all__'


class SalonCardSerializer(serializers.ModelSerializer):
    brand_name = serializers.SerializerMethodField('get_brand_name', read_only=True)

    def get_brand_name(self, instance):
        brand = instance.brand.all().first()
        return brand.name

    class Meta:
        model = Salon
        fields = [
            'id',
            'brand_name',
            'lng',
            'lat',
            'name',
            'avatar',
            'address',
            'email',
            'phone_number',
            'google',
            'site',
            'site_salon',
        ]


class SalonOpenSerializer(serializers.ModelSerializer):
    brand_name = serializers.SerializerMethodField('get_brand_name', read_only=True)
    info = serializers.SerializerMethodField('get_info', read_only=True)

    def get_brand_name(self, instance):
        brand = instance.brand.all().first()
        return brand.name

    def get_info(self, instance):
        packages = Package.objects.filter(model__brand=instance.brand.all().first()).order_by('fiyat')

        fiyat_min_package = packages.first()
        fiyat_max_package = packages.last()
        data = {
            'package_count': packages.count(),
            'model_count': packages.values('model_id').distinct().count(),
            'fiyat_min': fiyat_min_package.fiyat if fiyat_min_package else 0,
            'fiyat_max': fiyat_max_package.fiyat if fiyat_max_package else 0,
        }
        return data

    class Meta:
        model = Salon
        fields = [
            'id',
            'brand_name',
            'lng',
            'lat',
            'name',
            'avatar',
            'address',
            'email',
            'phone_number',
            'google',
            'site',
            'site_salon',
            'info',
        ]

