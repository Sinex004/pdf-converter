from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.db import models
from django.db.models import Manager


class CustomManager(Manager):
    def custom_filter(self, *args, **kwargs):
        updated_kwargs = {k: v for k, v in kwargs.items() if v is not None}
        return super().filter(*args, **updated_kwargs)


class Brand(models.Model):
    name = models.CharField(
        max_length=100,
        verbose_name='Marka',
        help_text='Бренд',
    )
    order = models.IntegerField(
        verbose_name='Цифра для сортировки, по умолчанию',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Бренд'
        verbose_name_plural = 'Бренды'

    def __str__(self):
        return self.name


class Model(models.Model):
    objects = CustomManager()

    def validate_image(fieldfile_obj):
        filesize = fieldfile_obj.size
        megabyte_limit = 1.0
        if filesize > megabyte_limit * 1024 * 1024:
            raise ValidationError("Max file size is %sMB" % str(megabyte_limit))

    BODY_OPTIONS = [
        ('sedan', 'sedan'),
        ('hatchback', 'hatchback'),
        ('MPV', 'MPV'),
        ('SUV', 'SUV')
    ]
    TYPE_OPTIONS = [
        ('Araba', 'Araba'),
        ('Ticari araç', 'Ticari araç'),
    ]
    brand = models.ForeignKey(
        Brand,
        on_delete=models.CASCADE,
        verbose_name='Marka',
        help_text='Бренд',
    )
    ready = models.BooleanField(
        verbose_name='Готово для публикации',
        default=False
    )
    name = models.CharField(
        max_length=100,
        verbose_name='Model',
        help_text='Модель',
    )
    type = models.CharField(
        max_length=100,
        verbose_name='Тип машины',
        help_text='Тип машины',
        choices=TYPE_OPTIONS,
    )
    body = models.CharField(
        max_length=10,
        verbose_name='Karoser',
        help_text='Кузов',
        choices=BODY_OPTIONS,
    )
    years = models.IntegerField(
        verbose_name='Garanti süresi, yıl',
        null=True,
        blank=True,
        
    )
    kms = models.IntegerField(
        verbose_name='Garanti süresi, km',
        null=True,
        blank=True,
    )
    link = models.CharField(
        max_length=2000,
        verbose_name='Ссылка на модель',
        help_text='Ссылка на модель на сайте производителя',
        default='',
        null=True,
        blank=True,
    )
    comment = models.CharField(
        max_length=2000,
        verbose_name='Комментарии при заполнении',
        help_text='Комментарии',
        default=''
    )
    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        verbose_name='Автор',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Модель'
        verbose_name_plural = 'Модели'

    def __str__(self):
        return f'{self.id}'


class Gallery(models.Model):
    def validate_image(fieldfile_obj):
        filesize = fieldfile_obj.size
        megabyte_limit = 2.0
        if filesize > megabyte_limit * 1024 * 1024:
            raise ValidationError("Max file size is %sMB" % str(megabyte_limit))

    IS_FIRST_OPTIONS = [
        ('Да', 'Да'),
        ('Нет', 'Нет')
    ]
    model = models.ForeignKey(
        Model,
        on_delete=models.CASCADE,
        verbose_name='Model',
        help_text='Модель',
    )
    image = models.ImageField(
        verbose_name='Фото',
        upload_to="gallery/",
        validators=[validate_image],
        help_text='Картинка не должна весить больше 2мб',
    )
    is_first = models.CharField(
        max_length=10,
        verbose_name='Главное фото',
        default='Нет',
        choices=IS_FIRST_OPTIONS,
    )
    card_300_250 = models.CharField(
        max_length=10,
        verbose_name='Карточка 300 250',
        default='Нет',
        choices=IS_FIRST_OPTIONS,
    )
    card_600_350 = models.CharField(
        max_length=10,
        verbose_name='Карточка 600 350',
        default='Нет',
        choices=IS_FIRST_OPTIONS,
    )

    class Meta:
        verbose_name = 'Фото'
        verbose_name_plural = 'Галерея'
        ordering = ['is_first']

    def __str__(self):
        return f'{self.model.name}'


class EuroNcap(models.Model):
    RATE_OPTIONS = [
        (0, 0),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
    ]
    model = models.ForeignKey(
        Model,
        on_delete=models.CASCADE,
        verbose_name='Model',
        help_text='Модель',
    )
    rate = models.IntegerField(
        verbose_name='Euro Ncap, yıldızlar',
        choices=RATE_OPTIONS,
        null=True,
        blank=True,
        
    )
    adult_pass = models.IntegerField(
        verbose_name='Yetişkin yolcu güvenliği, %',
        null=True,
        blank=True,
        
    )
    child_pass = models.IntegerField(
        verbose_name='Çocuk Yolcu Güvenliği, %',
        null=True,
        blank=True,
        
    )
    pedestrian = models.IntegerField(
        verbose_name='Yaya güvenliği, %',
        null=True,
        blank=True,
        
    )
    assistant_work = models.IntegerField(
        verbose_name='Güvenlik asistanlarının çalışmaları, %',
        null=True,
        blank=True,
        
    )
    link = models.CharField(
        max_length=500,
        verbose_name='Ссылка на отчет',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'EuroNcap'
        verbose_name_plural = 'EuroNcap'

    def __str__(self):
        return f'{self.model.name}'


class Package(models.Model):
    YEAR_OPTIONS = [
        (2020, 2020),
        (2021, 2021),
        (2022, 2022),
        (2023, 2023),
        (2024, 2024),
    ]
    SITES_OPTIONS = [
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10),
    ]
    TRANSMISSION_OPTIONS = [
        ('otomatik', 'otomatik'),
        ('manuel', 'manuel'),
    ]
    FUEL_OPTIONS = [
        ('benzin', 'benzin'),
        ('dizel', 'dizel'),
        ('elektrik', 'elektrik'),
        ('lpg', 'lpg'),
        ('hibrit benzin & lpg', 'hibrit benzin & lpg'),
        ('hibrit benzin & elektrik', 'hibrit benzin & elektrik'),
        ('hibrit dizel & elektrik', 'hibrit dizel & elektrik'),
    ]
    model = models.ForeignKey(
        Model,
        on_delete=models.CASCADE,
        verbose_name='Model',
        help_text='Модель',
    )
    ready = models.BooleanField(
        verbose_name='Готово для публикации',
        default=False
    )
    name = models.CharField(
        max_length=100,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    fiyat = models.FloatField(
        verbose_name='Fiyat, TRY',
        help_text='Цена',
        blank=True,
        null=True
    )
    order = models.BooleanField(
        verbose_name='Предзаказ',
        default=False
    )
    fuel_index = models.CharField(
        max_length=100,
        verbose_name='Motor indeksi',
        help_text='Индекс двигателя',
    )
    fuel_type = models.CharField(
        max_length=25,
        verbose_name='Yakıt',
        help_text='Топливо',
        choices=FUEL_OPTIONS,
    )
    transmiss = models.CharField(
        max_length=100,
        verbose_name='Vites Kutusu',
        help_text='Коробка передач',
        choices=TRANSMISSION_OPTIONS,
    )
    year = models.IntegerField(
        verbose_name='Model yılı',
        help_text='Год выпуска',
        default=2022,
        choices=YEAR_OPTIONS,
    )
    sites = models.IntegerField(
        verbose_name='Koltuk sayısı',
        help_text='Количество посадочных мест',
        choices=SITES_OPTIONS,
        null=True,
        blank=True,
    )
    comment = models.CharField(
        max_length=2000,
        verbose_name='Комментарии при заполнении',
        help_text='Комментарии',
        default=''
    )
    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        verbose_name='Автор',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Комплектация'
        verbose_name_plural = 'Комплектации'
        ordering = ['fiyat', 'name']

    def __str__(self):
        return self.name


class Motor(models.Model):
    BODY_OPTIONS = [
        ('sedan', 'sedan'),
        ('hatchback', 'hatchback'),
        ('MPV', 'MPV'),
        ('SUV', 'SUV')
    ]
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    capacity_l = models.FloatField(
        verbose_name='Motor hacmi, l',
        help_text='Объем двигателя в литрах, разделитель ","',
    )
    capacity_cc = models.IntegerField(
        verbose_name='Motor hacmi, cc',
        help_text='Объем двигателя в куб.см, без пробелов',
    )
    power_hp = models.IntegerField(
        verbose_name='Motor gücü, hp',
        help_text='Мощность двигателя в лошадиных силах',
    )
    power_rpm = models.IntegerField(
        verbose_name='Motor gücü, rpm',
        help_text='Мощность двигателя в оборотах',
    )
    power_kw = models.FloatField(
        verbose_name='Motor gücü, kW',
        help_text='Мощность двигателя в киловатах',
        null=True,
        blank=True,
    )
    torque = models.IntegerField(
        verbose_name='Tork, Nm',
        help_text='Куртящий момент в Nm',
    )
    torque_min = models.IntegerField(
        verbose_name='Min. Tork, rpm',
        help_text='Нижний порог оборотов макс. крутящего момента',
        null=True,
        blank=True,
        
    )
    torque_max = models.IntegerField(
        verbose_name='Max. Tork, rpm',
        help_text='Верхний порог оборотов макс. крутящего момента',
    )
    cylinder = models.IntegerField(
        verbose_name='Silindir adedi',
        help_text='Количество цилиндров',
    )
    valve = models.IntegerField(
        verbose_name='Valf adedi',
        help_text='Количество клапанов',
        null=True,
        blank=True,
        
    )
    compression = models.FloatField(
        verbose_name='Sıkıştırma Oranı',
        help_text='Степень сжатия',
        null=True,
        blank=True,
        
    )
    piston = models.CharField(
        max_length=30,
        verbose_name='Çap x Strok, mm',
        help_text='Диаметр и ход поршня',
        null=True,
        blank=True,
        
    )
    fuel_system = models.CharField(
        max_length=100,
        verbose_name='Yakıt sistemi',
        help_text='Топливная система',
        null=True,
        blank=True,
        
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Двигатель'
        verbose_name_plural = 'Двигатель'

    def __str__(self):
        return f'{self.package.name} - Двигатель'


class Electric(models.Model):
    MOTOR_OPTIONS = [
        ('N/A', 'N/A'),
        ('MHEV', 'MHEV'),
        ('PHEV', 'PHEV'),
        ('HEV', 'HEV'),
    ]
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    e_capacity_a = models.FloatField(
        verbose_name='Motor hacmi, l',
        help_text='Объем двигателя в литрах, разделитель ","',
        null=True,
        blank=True,
    )
    e_capacity_kw = models.FloatField(
        verbose_name='Motor hacmi, cc',
        help_text='Объем двигателя в куб.см, без пробелов',
        null=True,
        blank=True,
    )
    e_power_hp = models.FloatField(
        verbose_name='Motor gücü, hp',
        help_text='Мощность двигателя в лошадиных силах',
        null=True,
        blank=True,
    )
    e_power_kw = models.FloatField(
        verbose_name='Motor gücü, kW',
        help_text='Мощность двигателя в киловатах',
        null=True,
        blank=True,
    )
    e_power_front_kw = models.FloatField(
        verbose_name='ön Elektrikli Motor Maksimum güç, kW',
        help_text='Мощность переднего электро двигателя в киловатах',
        null=True,
        blank=True,
    )
    e_power_back_kw = models.FloatField(
        verbose_name='arka Elektrikli Motor Maksimum güç, kW',
        help_text='Мощность заднего электро двигателя в киловатах',
        null=True,
        blank=True,
    )
    e_torque = models.IntegerField(
        verbose_name='Tork, Nm',
        help_text='Куртящий момент в Nm',
        null=True,
        blank=True,
    )
    e_torque_min = models.IntegerField(
        verbose_name='Min. Tork, rpm',
        help_text='Нижний порог оборотов макс. крутящего момента',
        null=True,
        blank=True,

    )
    e_torque_max = models.IntegerField(
        verbose_name='Max. Tork, rpm',
        help_text='Верхний порог оборотов макс. крутящего момента',
        null=True,
        blank=True,
    )
    e_hybrid_type = models.CharField(
        max_length=200,
        verbose_name='Hibrit Sistem Aküsü Tip',
        help_text='Тип гибридной системы',
        null=True,
        blank=True,
    )
    e_battery_type = models.CharField(
        max_length=200,
        verbose_name='Hibrit Sistem Pil Tipi',
        help_text='Тип батареи',
        null=True,
        blank=True,
    )
    e_motor_type = models.CharField(
        max_length=200,
        verbose_name='Elektrikli Motor Tip',
        help_text='Тип системы электродвигателя',
        null=True,
        blank=True,

    )
    hybrid_type = models.CharField(
        max_length=200,
        verbose_name='hibrit tipi',
        help_text='Тип гибрида',
        choices=MOTOR_OPTIONS,
        null=True,
        blank=True,
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,

    )

    class Meta:
        verbose_name = 'Электрический двигатель'
        verbose_name_plural = 'Электрический Двигатель'

    def __str__(self):
        return f'{self.package.name} - Электрический Двигатель'


class Hybrid(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    hybrid_power_hp = models.IntegerField(
        verbose_name='Hibrit sistem gücü, hp',
        help_text='Мощность двигателя в лошадиных силах',
        null=True,
        blank=True,
    )
    hybrid_torque = models.IntegerField(
        verbose_name='Hibrit sistem tork, Nm',
        help_text='Куртящий момент в Nm',
        null=True,
        blank=True,
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
    )

    class Meta:
        verbose_name = 'Гибрид'
        verbose_name_plural = 'Гибрид'

    def __str__(self):
        return f'{self.package.name} - Гибрид'


class Fuel(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    consumption = models.FloatField(
        verbose_name='Kombine tüketim, L/100 km',
        help_text='Расход в смешанном цикле',
    )
    consumption_city = models.FloatField(
        verbose_name='Şehir içi tüketim,  L/100 km',
        help_text='Расход в городе',
        null=True,
        blank=True,
        
    )
    consumption_road = models.FloatField(
        verbose_name='Yoldaki tüketim,  L/100 km',
        help_text='Расход на трассе',
        null=True,
        blank=True,
        
    )
    tank = models.IntegerField(
        verbose_name='Yakıt Deposu, L',
        help_text='Объем бака',
    )
    co2_min = models.IntegerField(
        verbose_name='Min. CO2 Emisyonları, g/km',
        help_text='Минимальное значение выбросов СО2',
        null=True,
        blank=True,
        
    )
    co2_max = models.IntegerField(
        verbose_name='Max. CO2 Emisyonları, g/km',
        help_text='Максимальное значение выбросов СО2',
    )
    emission_standard = models.CharField(
        max_length=100,
        verbose_name='Emisyon normu',
        help_text='Название нормы выбросов',
        null=True,
        blank=True,
        
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Расход топлива и экологичность'
        verbose_name_plural = 'Расход топлива и экологичность'

    def __str__(self):
        return f'{self.package.name} - Расход топлива и экологичность'


class Control(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    acceleration = models.FloatField(
        verbose_name='Hızlanma 0-100 km/h, s',
        help_text='Разгон 0 - 100',
    )
    speed = models.IntegerField(
        verbose_name='Maksimum hız, km/h',
        help_text='Масимальная скорость',
    )
    turn_radius = models.FloatField(
        verbose_name='Dönüş yarıçapı, m',
        help_text='Радиус разворота, внимательно некоторые пишут диаметр разворота, тогда нужно делить на 2',
        null=True,
        blank=True,
        
    )
    wheel_turns = models.FloatField(
        verbose_name='Direksiyon kilitten kilide dönüyor',
        help_text='Обороты руля от упор до упора',
        null=True,
        blank=True,
        
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Разгон и Управление'
        verbose_name_plural = 'Разгон и Управление'

    def __str__(self):
        return f'{self.package.name} - Разгон и Управление'


class Transmission(models.Model):
    GEARS_OPTIONS = [
        (0, 0),
        (1, 1),
        (2, 2),
        (3, 3),
        (4, 4),
        (5, 5),
        (6, 6),
        (7, 7),
        (8, 8),
        (9, 9),
        (10, 10),
    ]
    TRANSMISSION_OPTIONS = [
        ('AT', 'AT'),
        ('MT', 'MT'),
        ('CVT', 'CVT'),
        ('DCT', 'DCT'),
        ('EDC', 'EDC'),
        ('DSG', 'DSG'),
        ('AMT', 'AMT'),
        ('AT Tiptronic', 'AT Tiptronic'),
        ('X-Tronic CVT', 'X-Tronic CVT'),
        ('Multidrive S CVT', 'Multidrive S CVT'),
        ('e-CVT', 'e-CVT'),
        ('Electric', 'Electric'),
        ('AT S tronic', 'AT S tronic'),
        ('AT Steptronic', 'AT Steptronic'),
        ('AT Geartronic', 'AT Geartronic'),
        ('G-Tronic', 'G-Tronic'),
        ('TCT', 'TCT'),
        ('PDK', 'PDK'),
        ('CVT Lineartronic', 'CVT Lineartronic'),
        ('LDF', ' LDF'),
    ]
    DRIVE_OPTIONS = [
        ('Ön', 'Ön'),
        ('Arka', 'Arka'),
        ('4x4', '4x4'),
    ]
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    transmission = models.CharField(
        max_length=30,
        verbose_name='Vites Kutusu',
        help_text='Коробка передач',
        choices=TRANSMISSION_OPTIONS
    )
    clutch = models.CharField(
        max_length=200,
        verbose_name='Debriyaj Sistemi',
        help_text='Тип сцепления',
        null=True,
        blank=True,

    )
    gears = models.IntegerField(
        verbose_name='Vites sayısı',
        help_text='Количество передач',
        choices=GEARS_OPTIONS,
    )
    drive = models.CharField(
        max_length=5,
        verbose_name='Çekiş',
        help_text='Привод',
        choices=DRIVE_OPTIONS,
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Привод и Трансмиссия'
        verbose_name_plural = 'Привод и Трансмиссия'

    def __str__(self):
        return f'{self.package.name} - Привод и Трансмиссия'


class Suspension(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    brake_front = models.CharField(
        max_length=40,
        verbose_name='Ön Fren',
        help_text='Передние тормоза',
        null=True,
        blank=True,
        
    )
    brake_back = models.CharField(
        max_length=40,
        verbose_name='Arka Fren',
        help_text='Задние тормоза',
        null=True,
        blank=True,
        
    )
    front = models.CharField(
        max_length=200,
        verbose_name='Ön Süspansiyon',
        help_text='Передняя подвеска',
        null=True,
        blank=True,
        
    )
    back = models.CharField(
        max_length=200,
        verbose_name='Arka Süspansiyon',
        help_text='Задняя подвеска',
        null=True,
        blank=True,
        
    )
    front_damper = models.CharField(
        max_length=200,
        verbose_name='Ön Süspansiyon',
        help_text='Передняя амортизаторы',
        null=True,
        blank=True,

    )
    back_damper = models.CharField(
        max_length=200,
        verbose_name='Arka Süspansiyon',
        help_text='Задняя амортизаторы',
        null=True,
        blank=True,

    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Тормоза и Подвеска'
        verbose_name_plural = 'Тормоза и Подвеска'

    def __str__(self):
        return f'{self.package.name} - Тормоза и Подвеска'


class Dimension(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    length = models.IntegerField(
        verbose_name='Uzunluk, mm',
        help_text='Длина',
    )
    width = models.IntegerField(
        verbose_name='Genişlik, mm',
        help_text='Ширина',
        null=True,
        blank=True,
        
    )
    height = models.IntegerField(
        verbose_name='Yükseklik, mm',
        help_text='Высота',
    )
    wheel_base = models.IntegerField(
        verbose_name='Aks Mesafesi, mm',
        help_text='Колесная база, расстояние между осями',
        null=True,
        blank=True,
        
    )
    width_front = models.IntegerField(
        verbose_name='Ön İz Genişliği, mm',
        help_text='Ширина передней колеи',
        null=True,
        blank=True,
        
    )
    width_back = models.IntegerField(
        verbose_name='Arka İz Genişliği, mm',
        help_text='Ширина задней колеи',
        null=True,
        blank=True,
        
    )
    overhang_front = models.IntegerField(
        verbose_name='Ön uzantı, mm',
        help_text='Передний свес',
        null=True,
        blank=True,
        
    )
    overhang_back = models.IntegerField(
        verbose_name='Arka uzantı, mm',
        help_text='Задний свес',
        null=True,
        blank=True,
        
    )
    trunk = models.IntegerField(
        verbose_name='Bagaj Hacmi, L',
        help_text='Объем багажника',
    )
    trunk_saloon = models.IntegerField(
        verbose_name='Bagaj hacmi, arka koltuklar en önde, L',
        help_text='Объем багажника с задними сиденьями подвинутыми вперед',
        null=True,
        blank=True,
    )
    trunk_second = models.IntegerField(
        verbose_name='Bagaj Hacmi 2. sıra koltuklar yatırıldığında, L',
        help_text='Объем багажника со сложенным вторым рядом',
        null=True,
        blank=True,
        
    )
    weight_driver = models.IntegerField(
        verbose_name='Sürücü Dahil Ağırlık, kg',
        help_text='Снаряженная масса с водителем',
        null=True,
        blank=True,
        
    )
    weight_pass = models.IntegerField(
        verbose_name='Azami yüklü ağırlık, kg',
        help_text='Максимальный вес авто с грузом и пассажирами',
        null=True,
        blank=True,
        
    )
    trailer = models.IntegerField(
        verbose_name='Frensiz treyler ağırlığı, kg',
        help_text='Вес прицепа без тормозов',
        null=True,
        blank=True,

    )
    trailer_brakes = models.IntegerField(
        verbose_name='Frenli treyler ağırlığı, kg',
        help_text='Вес прицепа с тормозами',
        null=True,
        blank=True,
        
    )
    tires_front = models.CharField(
        max_length=30,
        verbose_name='Ön lastikler',
        help_text='Передние шины',
        null=True,
        blank=True,
        
    )
    tires_back = models.CharField(
        max_length=30,
        verbose_name='Arka lastikler',
        help_text='Задние шины',
        null=True,
        blank=True,
        
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Размеры и Вес'
        verbose_name_plural = 'Размеры и Вес'

    def __str__(self):
        return f'{self.package.name} - Размеры и Вес'


class OffRoad(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    clearance = models.IntegerField(
        verbose_name='Yerden yükseklik, mm',
        help_text='Дорожный просвет (клиренс)',
        null=True,
        blank=True,
        
    )
    offroad_clearance = models.IntegerField(
        verbose_name='off-road yerden yükseklik , mm',
        help_text='Дорожный просвет внедорожный (клиренс)',
        null=True,
        blank=True,

    )
    angle_front = models.FloatField(
        verbose_name='Yaklaşma açısı, º',
        help_text='Угол въезда (угол сближения)',
        null=True,
        blank=True,
        
    )
    angle_back = models.FloatField(
        verbose_name='Uzaklaşma açısı, º',
        help_text='Угол съезда (угол отдаления)',
        null=True,
        blank=True,
        
    )
    angle_ramp = models.FloatField(
        verbose_name='Tepe açısı, º',
        help_text='Угол рампа, пиковый угол, верхний угол',
        null=True,
        blank=True,
        
    )
    depth = models.IntegerField(
        verbose_name='Sudan geçme yüksekliği, mm',
        help_text='Глубина брода',
        null=True,
        blank=True,
        
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Внедорожные характеристики'
        verbose_name_plural = 'Внедорожные характеристики'

    def __str__(self):
        return f'{self.package.name} - Внедорожные характеристики'


class Still(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Дизайн и стиль, сюда пишем все что относится к внешнему облику автомобиля'
        verbose_name_plural = 'Дизайн и стиль, сюда пишем все что относится к внешнему облику автомобиля'

    def __str__(self):
        return f'{self.package.name} - Дизайн и стиль, сюда пишем все что относится к внешнему облику автомобиля'


class Comfort(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Сюда пишем все что внутри машины, комфорт, функциональность, интерьер, мультимедиа'
        verbose_name_plural = 'Сюда пишем все что внутри машины, комфорт, функциональность, интерьер, мультимедиа'

    def __str__(self):
        return f'{self.package.name} - Сюда пишем все что внутри машины, комфорт, функциональность, интерьер, мультимедиа'


class Security(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        verbose_name = 'Безопасность'
        verbose_name_plural = 'Безопасность'

    def __str__(self):
        return f'{self.package.name} - Безопасность'


class Paket(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    name = models.CharField(
        max_length=300,
        verbose_name='Paket adı',
        help_text='Название пакета',
    )
    cost = models.IntegerField(
        verbose_name='Fiyat',
        help_text='Цена',
    )
    additional = models.JSONField(
        verbose_name='Дополнительные',
        null=True,
        blank=True,
        
    )

    class Meta:
        ordering = ['cost']
        verbose_name = 'Пакет'
        verbose_name_plural = 'Пакет'

    def __str__(self):
        return f'{self.package.name} - Пакет'


class Option(models.Model):
    package = models.ForeignKey(
        Package,
        on_delete=models.CASCADE,
        verbose_name='Versiyon',
        help_text='Комплектация',
    )
    name = models.CharField(
        max_length=300,
        verbose_name='Seçenek adı',
        help_text='Название опции',
    )
    cost = models.IntegerField(
        verbose_name='Fiyat',
        help_text='Цена',
    )

    class Meta:
        verbose_name = 'Опция'
        verbose_name_plural = 'Опция'

    def __str__(self):
        return f'{self.package.name} - Опция'


class City(models.Model):
    name = models.CharField(
        max_length=150,
        verbose_name='Название на турецком'
    )
    name_eng = models.CharField(
        max_length=150,
        verbose_name='Название на английском',
        null=True,
        blank=True
    )

    class Meta:
        verbose_name = 'Город'
        verbose_name_plural = 'Города'
        ordering = ['name']

    def __str__(self):
        return self.name


class Salon(models.Model):
    def validate_image(fieldfile_obj):
        filesize = fieldfile_obj.file.size
        megabyte_limit = 2.0
        if filesize > megabyte_limit * 1024 * 1024:
            raise ValidationError("Max file size is %sMB" % str(megabyte_limit))

    brand = models.ManyToManyField(
        Brand,
        verbose_name='Бренды',
    )
    city = models.ForeignKey(
        City,
        on_delete=models.CASCADE,
        verbose_name='Город',
    )
    name = models.CharField(
        max_length=150,
        verbose_name='Название'
    )
    address = models.CharField(
        max_length=200,
        verbose_name='Адрес'
    )
    google = models.CharField(
        max_length=500,
        verbose_name='Ссылка на гугл'
    )
    lat = models.FloatField(
        verbose_name='latitude, широта',
        null=True,
        blank=True,
    )
    lng = models.FloatField(
        verbose_name='longitude, долгота',
        null=True,
        blank=True,
    )
    site = models.CharField(
        max_length=500,
        verbose_name='Сайт диллера от производителя',
        help_text='Берется с сайта производителя',
        null=True,
        blank=True,
        
    )
    phone_number = models.CharField(
        max_length=20,
        verbose_name='Мобильный телефон',
        null=True,
        blank=True,
        
    )
    city_number = models.CharField(
        max_length=20,
        verbose_name='Городской телефон',
        null=True,
        blank=True,
        
    )
    email = models.CharField(
        max_length=100,
        verbose_name='Почта диллера',
        null=True,
        blank=True,
        
    )
    email_captain = models.CharField(
        max_length=100,
        verbose_name='Почта руководства',
        null=True,
        blank=True,
        
    )
    email_sale = models.CharField(
        max_length=100,
        verbose_name='Почта отдел продаж',
        null=True,
        blank=True,
        
    )
    email_marketing = models.CharField(
        max_length=100,
        verbose_name='Почта отдел маркетинга',
        null=True,
        blank=True,
        
    )
    avatar = models.ImageField(
        verbose_name='Фото',
        upload_to="salon/",
        validators=[validate_image],
        help_text='Картинка не должна весить больше 2мб',
    )
    site_salon = models.CharField(
        max_length=500,
        verbose_name='Сайт автосалона',
        help_text='Берется с google maps',
        blank=True,
        null=True
    )
    email_salon = models.CharField(
        max_length=100,
        verbose_name='Почта салона',
        null=True,
        blank=True,
        
    )
    # Дата создания
    created_at = models.DateTimeField(
        auto_now_add=True,
        verbose_name='Дата создания',
        null=True,
    )
    created_by = models.ForeignKey(
        User,
        on_delete=models.SET_NULL,
        verbose_name='Автор',
        null=True,
        blank=True,
        )

    class Meta:
        verbose_name = 'Автосалон'
        verbose_name_plural = 'Автосалоны'

    def __str__(self):
        return self.name


class Rating(models.Model):
    model = models.ForeignKey(
        Model,
        verbose_name='Модель',
        on_delete=models.CASCADE,
    )
    user = models.ForeignKey(
        User,
        verbose_name='Автор',
        on_delete=models.CASCADE,
    )
    score = models.IntegerField(
        verbose_name='Рейтинг',
    )
    created_at = models.DateTimeField(
        auto_now_add=True
    )

    class Meta:
        verbose_name = 'Рейтинг'
        verbose_name_plural = 'Рейтинги'
        ordering = ['created_at']

    def __str__(self):
        return self.user.username


class Comment(models.Model):
    model = models.ForeignKey(
        Model,
        verbose_name='Модель',
        on_delete=models.CASCADE,
        related_name='comments'
    )
    comment = models.ForeignKey(
        "self",
        verbose_name='Главный комментарий',
        on_delete=models.SET_NULL,
        null=True,
        related_name='answers'
    )
    reply_to = models.ForeignKey(
        User,
        verbose_name='Автор',
        on_delete=models.SET_NULL,
        null=True,
        related_name='reply_to_set'
    )
    user = models.ForeignKey(
        User,
        verbose_name='Автор',
        on_delete=models.CASCADE,
    )
    message = models.TextField()
    created_at = models.DateTimeField(
        auto_now_add=True
    )
    operations_count = models.IntegerField(
        verbose_name='Количество лайков',
        default=0,
    )
    likes = models.ManyToManyField(User, related_name='comment_likes', blank=True)
    dislikes = models.ManyToManyField(User, related_name='comment_dislikes', blank=True)

    class Meta:
        verbose_name = 'Комментарий'
        verbose_name_plural = 'Комментарии'
        ordering = ['created_at']

    def __str__(self):
        return self.user.username