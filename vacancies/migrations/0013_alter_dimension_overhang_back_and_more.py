# Generated by Django 4.0.4 on 2022-08-23 09:16

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vacancies', '0012_model_avatar_model_body_model_created_at_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='dimension',
            name='overhang_back',
            field=models.IntegerField(blank=True, help_text='Задний свес', null=True, verbose_name='Arka uzantı, mm'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='overhang_front',
            field=models.IntegerField(blank=True, help_text='Передний свес', null=True, verbose_name='Ön uzantı, mm'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='tires_back',
            field=models.CharField(blank=True, help_text='Задние шины', max_length=30, null=True, verbose_name='Arka lastikler'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='tires_front',
            field=models.CharField(blank=True, help_text='Передние шины', max_length=30, null=True, verbose_name='Ön lastikler'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='trailer',
            field=models.IntegerField(blank=True, help_text='Вес прицепа без тормозов', null=True, verbose_name='Frensiz treyler ağırlığı, kg'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='trailer_brakes',
            field=models.IntegerField(blank=True, help_text='Вес прицепа с тормозами', null=True, verbose_name='Frenli treyler ağırlığı, kg'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='trunk_second',
            field=models.IntegerField(blank=True, help_text='Объем багажника со сложенным вторым рядом', null=True, verbose_name='Bagaj Hacmi 2. sıra koltuklar yatırıldığında, L'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='weight_driver',
            field=models.IntegerField(blank=True, help_text='Снаряженная масса с водителем', null=True, verbose_name='Boş Ağırlık, kg'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='weight_pass',
            field=models.IntegerField(blank=True, help_text='Максимальный вес авто с грузом и пассажирами', null=True, verbose_name='Azami yüklü ağırlık, kg'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='wheel_base',
            field=models.IntegerField(blank=True, help_text='Колесная база, расстояние между осями', null=True, verbose_name=''),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='width',
            field=models.IntegerField(blank=True, help_text='Ширина', null=True, verbose_name='Genişlik, mm'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='width_back',
            field=models.IntegerField(blank=True, help_text='Ширина задней колеи', null=True, verbose_name='Arka İz Genişliği, mm'),
        ),
        migrations.AlterField(
            model_name='dimension',
            name='width_front',
            field=models.IntegerField(blank=True, help_text='Ширина передней колеи', null=True, verbose_name='Ön İz Genişliği, mm'),
        ),
        migrations.AlterField(
            model_name='suspension',
            name='back',
            field=models.CharField(blank=True, help_text='Задняя подвеска', max_length=40, null=True, verbose_name='Arka Süspansiyon'),
        ),
        migrations.AlterField(
            model_name='suspension',
            name='brake_back',
            field=models.CharField(blank=True, help_text='Задние тормоза', max_length=40, null=True, verbose_name='Arka Fren'),
        ),
        migrations.AlterField(
            model_name='suspension',
            name='brake_front',
            field=models.CharField(blank=True, help_text='Передние тормоза', max_length=40, null=True, verbose_name='Ön Fren'),
        ),
        migrations.AlterField(
            model_name='suspension',
            name='front',
            field=models.CharField(blank=True, help_text='Передняя подвеска', max_length=40, null=True, verbose_name='Ön Süspansiyon'),
        ),
    ]
