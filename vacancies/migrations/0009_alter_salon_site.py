# Generated by Django 4.0.4 on 2022-08-03 03:29

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('vacancies', '0008_salon_email_salon_salon_site_salon_and_more'),
    ]

    operations = [
        migrations.AlterField(
            model_name='salon',
            name='site',
            field=models.CharField(blank=True, help_text='Берется с сайта производителя', max_length=500, null=True, verbose_name='Сайт диллера от производителя'),
        ),
    ]
