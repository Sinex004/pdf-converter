from datetime import datetime

from django.contrib.auth.models import User
from django.db.models import Count
from rest_framework import status
from rest_framework.authtoken.models import Token
from rest_framework.decorators import permission_classes, api_view
from rest_framework.permissions import IsAuthenticated, AllowAny
from rest_framework.response import Response

from common.telegram import send_message_to_user
from vacancies.models import Model, Brand, City, Salon, Package, Comment, Rating
from vacancies.serializers import (
    ModelSerializer, BrandSerializer, EuroNcapSerializer, GallerySerializer,
    PackageSerializer, MotorSerializer, FuelSerializer, ControlSerializer,
    TransmissionSerializer, SuspensionSerializer, DimensionSerializer, OffRoadSerializer, StillSerializer,
    ComfortSerializer, SecuritySerializer, PaketSerializer, OptionSerializer,
    ElectricSerializer, HybridSerializer,
    CitySerializer, ModelCardSerializer, BrandConfigSerializer, ModelOpenSerializer, ModelGallerySerializer,
    SalonCardSerializer, SalonOpenSerializer, CommentSerializer, PaketSerializer1
)


def check_city(city_name):
    if city_name:
        city = City.objects.filter(name_eng=city_name).first()
        if not city:
            city = City.objects.filter(name='İstanbul').first()
    else:
        city = City.objects.filter(name='İstanbul').first()
    return city


def filter_models(
        city=None, brand_name=None, body=None,
        fiyat_min=None, fiyat_max=None, transmiss=None, drive=None,
        fuel_type=None, capacity_l=None, acceleration=None, order=None
):
    if brand_name:
        brand_name = brand_name.replace('-', ' ')
    if body:
        if body == 'SUV':
            body = ['hatchback', 'SUV']
        else:
            body = [body]
    if fuel_type:
        if fuel_type == 'hibrit benzin lpg':
            fuel_type = 'hibrit benzin & lpg'
        elif fuel_type == 'hibrit benzin elektrik':
            fuel_type = 'hibrit benzin & elektrik'
        elif fuel_type == 'hibrit dizel elektrik':
            fuel_type = 'hibrit dizel & elektrik'
        else:
            fuel_type = fuel_type
    if capacity_l:
        if capacity_l == '0-1.3':
            capacity_l_min = None
            capacity_l_max = 1.3
        elif capacity_l == '1.3-1.6':
            capacity_l_min = 1.3
            capacity_l_max = 1.6
        elif capacity_l == '1.6-1.8':
            capacity_l_min = 1.6
            capacity_l_max = 1.8
        elif capacity_l == '1.8-2.0':
            capacity_l_min = 1.8
            capacity_l_max = 2.0
        elif capacity_l == '2.0-2.5':
            capacity_l_min = 2.0
            capacity_l_max = 2.5
        elif capacity_l == '2.5-3.0':
            capacity_l_min = 2.5
            capacity_l_max = 3.0
        elif capacity_l == '3.0-3.5':
            capacity_l_min = 3.0
            capacity_l_max = 3.5
        elif capacity_l == '3.5-4.0':
            capacity_l_min = 3.5
            capacity_l_max = 4.0
        elif capacity_l == '4.0':
            capacity_l_min = 4.0
            capacity_l_max = None
        else:
            capacity_l_min = None
            capacity_l_max = None
    else:
        capacity_l_min = None
        capacity_l_max = None

    if acceleration:
        if acceleration == '0-4':
            acceleration_min = None
            acceleration_max = 4
        elif acceleration == '4-6':
            acceleration_min = 4
            acceleration_max = 6
        elif acceleration == '6-8':
            acceleration_min = 6
            acceleration_max = 8
        elif acceleration == '8-10':
            acceleration_min = 8
            acceleration_max = 10
        elif acceleration == '10':
            acceleration_min = 10
            acceleration_max = None
        else:
            acceleration_min = None
            acceleration_max = None
    else:
        acceleration_min = None
        acceleration_max = None
    if order == 'Asc':
        models = Model.objects.custom_filter(
            brand__salon__city=city,
            brand__name=brand_name,
            body__in=body,
            package__fiyat__gte=fiyat_min,
            package__fiyat__lte=fiyat_max,
            package__transmiss=transmiss,
            package__transmission__drive=drive,
            package__fuel_type=fuel_type,
            package__motor__capacity_l__gte=capacity_l_min,
            package__motor__capacity_l__lt=capacity_l_max,
            package__control__acceleration__gte=acceleration_min,
            package__control__acceleration__lt=acceleration_max,
        ).distinct().order_by('id')
    elif order == 'Desc':
        models = Model.objects.custom_filter(
            brand__salon__city=city,
            brand__name=brand_name,
            body__in=body,
            package__fiyat__gte=fiyat_min,
            package__fiyat__lte=fiyat_max,
            package__transmiss=transmiss,
            package__transmission__drive=drive,
            package__fuel_type=fuel_type,
            package__motor__capacity_l__gte=capacity_l_min,
            package__motor__capacity_l__lt=capacity_l_max,
            package__control__acceleration__gte=acceleration_min,
            package__control__acceleration__lt=acceleration_max,
        ).distinct().order_by('-id')
    else:
        models = Model.objects.custom_filter(
            brand__salon__city=city,
            brand__name=brand_name,
            body__in=body,
            package__fiyat__gte=fiyat_min,
            package__fiyat__lte=fiyat_max,
            package__transmiss=transmiss,
            package__transmission__drive=drive,
            package__fuel_type=fuel_type,
            package__motor__capacity_l__gte=capacity_l_min,
            package__motor__capacity_l__lt=capacity_l_max,
            package__control__acceleration__gte=acceleration_min,
            package__control__acceleration__lt=acceleration_max,
        ).distinct()
    return models


@api_view(['GET'])
@permission_classes((AllowAny,))
def config_header(request):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    brands = Brand.objects.filter(salon__city=city).distinct()
    cars = {
        'count': Model.objects.filter(brand__salon__city=city).distinct().values('id').count(),
    }
    compare = {'count': 0}
    if request.user:
        profile = {
            'username': request.user.username,
            'comments': [
                {
                    'text': 'sometext 1'
                },
                {
                    'text': 'sometext 2'
                }
            ],
        }
    else:
        profile = {}
    result = {
        'cities': CitySerializer(City.objects.all(), many=True).data,
        'cars': cars,
        'compare': compare,
        'profile': profile,
        'city': CitySerializer(city).data,
        'brands': BrandConfigSerializer(brands.order_by('name'), many=True).data,
        'brands_top': BrandConfigSerializer(brands.order_by('order')[:15], many=True).data
    }
    return Response(result, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def city_models_list(request):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    body = request.query_params.get('body')
    fiyat_min = request.query_params.get('fiyat_min')
    fiyat_max = request.query_params.get('fiyat_max')
    transmiss = request.query_params.get('transmission')
    drive = request.query_params.get('drive')
    fuel_type = request.query_params.get('fuel')
    capacity_l = request.query_params.get('motor')
    acceleration = request.query_params.get('acceleration')
    order = request.query_params.get('filter')
    models = filter_models(
        city=city, brand_name=None, body=body,
        fiyat_min=fiyat_min, fiyat_max=fiyat_max, transmiss=transmiss, drive=drive,
        fuel_type=fuel_type, capacity_l=capacity_l, acceleration=acceleration, order=order
    )
    serializer = ModelCardSerializer(models, many=True)
    packages_count = Package.objects.filter(model__in=models.values_list('id', flat=True)).distinct().count()
    salons_count = Salon.objects.filter(brand__in=models.values_list('brand__id', flat=True).distinct(), city=city).distinct().count()
    return Response(
        {'filteredData': serializer.data, 'packages_count': packages_count, 'salons_count': salons_count},
        status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def brand_models_list(request, brand_name):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    body = request.query_params.get('body')
    fiyat_min = request.query_params.get('fiyat_min')
    fiyat_max = request.query_params.get('fiyat_max')
    transmiss = request.query_params.get('transmission')
    drive = request.query_params.get('drive')
    fuel_type = request.query_params.get('fuel')
    capacity_l = request.query_params.get('motor')
    acceleration = request.query_params.get('acceleration')
    order = request.query_params.get('filter')
    models = filter_models(
        city=city, brand_name=brand_name, body=body,
        fiyat_min=fiyat_min, fiyat_max=fiyat_max, transmiss=transmiss, drive=drive,
        fuel_type=fuel_type, capacity_l=capacity_l, acceleration=acceleration, order=order
    )
    serializer = ModelCardSerializer(models, many=True)
    packages_count = Package.objects.filter(model__in=models.values_list('id', flat=True)).distinct().count()
    salons_count = Salon.objects.filter(brand__in=models.values_list('brand__id', flat=True).distinct(), city=city).distinct().count()
    return Response(
        {'filteredData': serializer.data, 'packages_count': packages_count, 'salons_count': salons_count},
        status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def model_detail(request, brand_name, model_name, type_tab):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    brand_name = brand_name.replace('-', ' ')
    model_name = model_name.replace('-', ' ')
    model = Model.objects.prefetch_related(
        'brand__salon_set', 'gallery_set', 'package_set', 'euroncap_set', 'comments'
    ).get(name=model_name, brand__name=brand_name)
    if model.brand.salon_set.filter(city=city):
        serializer = ModelOpenSerializer(model, context={'city': city, 'user': request.user})
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        serializer = ModelGallerySerializer(model)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def model_detail_copy(request, brand_name, model_name, type_tab, model_package, package_name):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    brand_name = brand_name.replace('-', ' ')
    model_name = model_name.replace('-', ' ')
    model = Model.objects.prefetch_related(
        'brand__salon_set', 'gallery_set', 'package_set', 'euroncap_set', 'comments'
    ).get(name=model_name, brand__name=brand_name)
    if model.brand.salon_set.filter(city=city):
        serializer = ModelOpenSerializer(model, context={'city': city, 'user': request.user})
        return Response(serializer.data, status=status.HTTP_200_OK)
    else:
        serializer = ModelGallerySerializer(model)
        return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def salons_list(request):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    serializer = SalonCardSerializer(Salon.objects.filter(city=city), many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def brand_salons_list(request, brand_name):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    brand_name = brand_name.replace('-', ' ')
    serializer = SalonCardSerializer(Salon.objects.filter(city=city, brand__name=brand_name), many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def salon_detail(request, brand_name, salon_id):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    brand_name = brand_name.replace('-', ' ')
    serializer = SalonOpenSerializer(Salon.objects.filter(
        brand__name=brand_name, city=city, id=salon_id,
    ).first())
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((AllowAny,))
def register_view(request):
    user = User(
        username=request.data['username'],
        email=request.data['email'],
    )
    user.save()
    user.set_password(request.data['password'])
    user.save()
    token, _ = Token.objects.get_or_create(user=user)
    return Response({"token": token.key}, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def comments_list(request, brand_name, model_name):
    city_name = request.query_params.get('city')
    city = check_city(city_name)
    if not city:
        return Response({'message': 'Нет query param "city", оно должно быть на английском'},
                        status=status.HTTP_400_BAD_REQUEST)
    brand_name = brand_name.replace('-', ' ')
    model_name = model_name.replace('-', ' ')
    comments = Comment.objects.prefetch_related(
        'answers'
    ).filter(model__name=model_name, model__brand__name=brand_name)
    serializer = CommentSerializer(comments, many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def comment_like(request, brand_name, model_name, comment_id):
    comment = Comment.objects.filter(id=comment_id).first()
    if not comment:
        return Response({'message': 'Комментарий не найден'},
                        status=status.HTTP_400_BAD_REQUEST)
    if comment.likes.filter(id=request.user.id).exists():
        comment.likes.remove(request.user)
        comment.operations_count = comment.operations_count - 1
    else:
        comment.likes.add(request.user)
        comment.operations_count = comment.operations_count + 1
        comment.save()
    return Response(status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def comment_dislike(request, brand_name, model_name, comment_id):
    comment = Comment.objects.filter(id=comment_id).first()
    if not comment:
        return Response({'message': 'Комментарий не найден'},
                        status=status.HTTP_400_BAD_REQUEST)
    if comment.dislikes.filter(id=request.user.id).exists():
        comment.dislikes.remove(request.user)
        comment.operations_count = comment.operations_count - 1
    else:
        comment.dislikes.add(request.user)
        comment.operations_count = comment.operations_count + 1
    comment.save()
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def rating_detail(request, brand_name, model_name):
    brand_name = brand_name.replace('-', ' ')
    model_name = model_name.replace('-', ' ')
    model = Model.objects.get(name=model_name, brand__name=brand_name)
    rating = Rating.objects.filter(
        model=model,
        user=request.user,
    ).first()
    if rating:
        rating.score = request.data['score']
    else:
        rating = Rating(
            model=model,
            user=request.user,
            score=request.data['score']
        )
    rating.save()
    return Response(status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def comment_detail(request, brand_name, model_name):
    brand_name = brand_name.replace('-', ' ')
    model_name = model_name.replace('-', ' ')
    if 'reply_to' in request.data:
        if request.data['reply_to']:
            reply_to = User.objects.filter(username=request.data['reply_to'])
        else:
            reply_to = None
    else:
        reply_to = None
    if 'comment_parent' in request.data:
        if request.data['comment_parent']:
            comment_parent = User.objects.filter(username=request.data['reply_to'])
        else:
            comment_parent = None
    else:
        comment_parent = None
    model = Model.objects.get(name=model_name, brand__name=brand_name)
    comment = Comment(
        model=model,
        message=request.data['message'],
        user=request.user,
        reply_to=reply_to,
        comment=comment_parent,
    )
    comment.save()
    return Response(status=status.HTTP_200_OK)

# ############################################################################################################


@api_view(['GET'])
@permission_classes((AllowAny,))
def models_list(request, brand_id):
    serializer = ModelSerializer(Model.objects.filter(brand_id=brand_id).order_by('-id'), many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['GET'])
@permission_classes((AllowAny,))
def brands_list(request):
    serializer = BrandSerializer(Brand.objects.all().order_by('name'), many=True)
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def brand_create(request):
    if request.method == 'POST':
        serializer = BrandSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def model_gallery(request, brand_id, model_id):
    if request.method == 'POST':
        data = request.FILES
        print('data')
        print(data)
        print('request.data')
        print(request.data)
        if data:
            for filename in data:
                print(filename)
                if "_main" in filename:
                    gallery_data = {
                        "model": model_id,
                        "image": data.getlist(filename)[0],
                        "is_first": "Да"
                    }
                    serializer_gallery = GallerySerializer(data=gallery_data)
                    if not serializer_gallery.is_valid():
                        Model.objects.filter(id=model_id).delete()
                        return Response(serializer_gallery.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_gallery.save()
                else:
                    for file in data.getlist(filename):
                        gallery_data = {
                            "model": model_id,
                            "image": file,
                        }
                        serializer_gallery = GallerySerializer(data=gallery_data)
                        if not serializer_gallery.is_valid():
                            Model.objects.filter(id=model_id).delete()
                            return Response(serializer_gallery.errors, status=status.HTTP_400_BAD_REQUEST)
                        serializer_gallery.save()
                # gallery_data = {
                #     "model": model_id,
                #     "image": file,
                # }
        return Response({"message": "Успешно создано"}, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def model_create(request, brand_id):
    if request.method == 'POST':
        data = request.data
        data['model']['brand'] = brand_id
        serializer_model = ModelSerializer(data=data['model'])
        if not serializer_model.is_valid():
            return Response(serializer_model.errors, status=status.HTTP_400_BAD_REQUEST)
        model = serializer_model.save(created_by=request.user)

        data['euroncap_set']['model'] = model.id
        if data['euroncap_set']:
            serializer_euro = EuroNcapSerializer(data=data['euroncap_set'])
            if not serializer_euro.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_euro.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_euro.save(model=model)
        send_message_to_user(
            "445951537",
            f'{request.user.username}\n'
            f'Начало создания модели {data["model"]["name"]} ------------\n'
        )
        for i in data['package_set']:
            if not i:
                continue
            i['model'] = model.id
            fields = [
                'motor_set',
                'fuel_set',
                'control_set',
                'transmission_set',
                'suspension_set',
                'dimension_set',
                'offroad_set',
                'still_set',
                'comfort_set',
                'security_set',
                'hybrid_set',
                'electric_set'
            ]
            temp = list(i.keys())
            for key in temp:
                if key not in fields:
                    if not i[key]:
                        del i[key]
            for set_name in fields:
                if set_name in i:
                    if i[set_name]:
                        temp1 = list(i[set_name].keys())
                        for key in temp1:
                            if not i[set_name][key]:
                                del i[set_name][key]
            if 'order' in i:
                if i['order'] == True or i['order'] == 'true':
                    del i['fiyat']
            serializer_package = PackageSerializer(data=i)
            if not serializer_package.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_package.errors, status=status.HTTP_400_BAD_REQUEST)
            package = serializer_package.save(model=model, created_by=request.user)

            i['motor_set']['package'] = package.id
            serializer_motor_set = MotorSerializer(data=i['motor_set'])
            if not serializer_motor_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_motor_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_motor_set.save(package=package)

            if i['fuel_type'] == 'hibrit benzin & elektrik' or i['fuel_type'] == 'hibrit dizel & elektrik':
                if 'electric_set' in i:
                    i['electric_set']['package'] = package.id
                    serializer_electric_set = ElectricSerializer(data=i['electric_set'])
                    if not serializer_electric_set.is_valid():
                        Model.objects.filter(id=model.id).delete()
                        return Response(serializer_electric_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_electric_set.save(package=package)
                if 'hybrid_set' in i:
                    i['hybrid_set']['package'] = package.id
                    serializer_hybrid_set = HybridSerializer(data=i['hybrid_set'])
                    if not serializer_hybrid_set.is_valid():
                        Model.objects.filter(id=model.id).delete()
                        return Response(serializer_hybrid_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_hybrid_set.save(package=package)
            i['fuel_set']['package'] = package.id
            serializer_fuel_set = FuelSerializer(data=i['fuel_set'])
            if not serializer_fuel_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_fuel_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_fuel_set.save(package=package)

            i['control_set']['package'] = package.id
            serializer_control_set = ControlSerializer(data=i['control_set'])
            if not serializer_control_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_control_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_control_set.save(package=package)

            i['transmission_set']['package'] = package.id
            serializer_transmission_set = TransmissionSerializer(data=i['transmission_set'])
            if not serializer_transmission_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_transmission_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_transmission_set.save(package=package)

            i['suspension_set']['package'] = package.id
            serializer_suspension_set = SuspensionSerializer(data=i['suspension_set'])
            if not serializer_suspension_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_suspension_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_suspension_set.save(package=package)

            i['dimension_set']['package'] = package.id
            serializer_dimension_set = DimensionSerializer(data=i['dimension_set'])
            if not serializer_dimension_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_dimension_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_dimension_set.save(package=package)

            i['offroad_set']['package'] = package.id
            serializer_offroad_set = OffRoadSerializer(data=i['offroad_set'])
            if not serializer_offroad_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_offroad_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_offroad_set.save(package=package)

            i['still_set']['package'] = package.id
            serializer_still_set = StillSerializer(data=i['still_set'])
            if not serializer_still_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_still_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_still_set.save(package=package)

            i['comfort_set']['package'] = package.id
            serializer_comfort_set = ComfortSerializer(data=i['comfort_set'])
            if not serializer_comfort_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_comfort_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_comfort_set.save(package=package)

            i['security_set']['package'] = package.id
            serializer_security_set = SecuritySerializer(data=i['security_set'])
            if not serializer_security_set.is_valid():
                Model.objects.filter(id=model.id).delete()
                return Response(serializer_security_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_security_set.save(package=package)

            if 'paket_set' in i:
                for j in i['paket_set'].values():
                    j['package'] = package.id
                    serializer_paket_set = PaketSerializer1(data=j)
                    if not serializer_paket_set.is_valid():
                        Model.objects.filter(id=model.id).delete()
                        return Response(serializer_paket_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_paket_set.save(package=package)
            if 'option_set' in i:
                for j in i['option_set'].values():
                    j['package'] = package.id
                    serializer_option_set = OptionSerializer(data=j)
                    if not serializer_option_set.is_valid():
                        Model.objects.filter(id=model.id).delete()
                        return Response(serializer_option_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_option_set.save(package=package)
            send_message_to_user(
                "445951537",
                f'{request.user.username}\n'
                f'{datetime.now()}\n'
                f'Создан пакет {i["name"]}\n'
                f'{i}'
            )
        send_message_to_user(
            "445951537",
            f'{request.user.username}\n'
            f'{datetime.now()}\n'
            f'Создана модель {data["model"]["name"]}'
        )
        return Response({"message": "Успешно создано", "model_id": model.id}, status=status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes((IsAuthenticated,))
def model_delete(request, brand_id, model_id):
    if request.method == 'GET':
        Model.objects.filter(id=model_id).delete()
        return Response({"message": "Успешно удалено"}, status=status.HTTP_201_CREATED)


@api_view(['GET'])
@permission_classes((AllowAny,))
def model_view(request, brand_id, model_id):
    serializer = ModelSerializer(Model.objects.get(id=model_id))
    return Response(serializer.data, status=status.HTTP_200_OK)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def model_gallery_edit(request, brand_id, model_id):
    if request.method == 'POST':
        model = Model.objects.filter(id=model_id).first()
        data = request.FILES
        print('request.FILES')
        print(data)
        print('request.data')
        print(request.data)
        if data:
            model.gallery_set.all().delete()
            for filename in data:
                if "_main" in filename:
                    gallery_data = {
                        "model": model_id,
                        "image": data.getlist(filename)[0],
                        "is_first": "Да"
                    }
                    serializer_gallery = GallerySerializer(data=gallery_data)
                    if not serializer_gallery.is_valid():
                        Model.objects.filter(id=model_id).delete()
                        return Response(serializer_gallery.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_gallery.save()
                else:
                    for file in data.getlist(filename):
                        gallery_data = {
                            "model": model_id,
                            "image": file,
                        }
                        serializer_gallery = GallerySerializer(data=gallery_data)
                        if not serializer_gallery.is_valid():
                            model.gallery_set.all().delete()
                            return Response(serializer_gallery.errors, status=status.HTTP_400_BAD_REQUEST)
                        serializer_gallery.save()
                # gallery_data = {
                #     "model": model_id,
                #     "image": file,
                # }
        return Response({"message": "Успешно создано"}, status=status.HTTP_201_CREATED)


@api_view(['POST'])
@permission_classes((IsAuthenticated,))
def model_edit(request, brand_id, model_id):
    if request.method == 'POST':
        data = request.data
        model = Model.objects.filter(id=model_id).first()
        model.package_set.all().delete()
        model.euroncap_set.all().delete()
        data['model']['brand'] = brand_id
        serializer_model = ModelSerializer(model, data=data['model'])
        if not serializer_model.is_valid():
            return Response(serializer_model.errors, status=status.HTTP_400_BAD_REQUEST)
        model_edited = serializer_model.save(created_by=request.user)

        data['euroncap_set']['model'] = model.id
        if data['euroncap_set']:
            serializer_euro = EuroNcapSerializer(data=data['euroncap_set'])
            if not serializer_euro.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_euro.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_euro.save(model=model)
        send_message_to_user(
            "445951537",
            f'{request.user.username}\n'
            f'Начало редактирования модели {data["model"]["name"]} ------------\n'
        )
        for i in data['package_set']:
            if not i:
                continue
            i['model'] = model.id
            fields = [
                'motor_set',
                'fuel_set',
                'control_set',
                'transmission_set',
                'suspension_set',
                'dimension_set',
                'offroad_set',
                'still_set',
                'comfort_set',
                'security_set',
                'hybrid_set',
                'electric_set'
            ]
            temp = list(i.keys())
            for key in temp:
                if key not in fields:
                    if not i[key]:
                        del i[key]
            for set_name in fields:
                if set_name in i:
                    if i[set_name]:
                        temp1 = list(i[set_name].keys())
                        for key in temp1:
                            if not i[set_name][key]:
                                del i[set_name][key]

            if 'order' in i:
                if i['order'] == True or i['order'] == 'true':
                    del i['fiyat']

            serializer_package = PackageSerializer(data=i)
            if not serializer_package.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_package.errors, status=status.HTTP_400_BAD_REQUEST)
            package = serializer_package.save(model=model, created_by=request.user)

            i['motor_set']['package'] = package.id
            serializer_motor_set = MotorSerializer(data=i['motor_set'])
            if not serializer_motor_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_motor_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_motor_set.save(package=package)
            if i['fuel_type'] == 'hibrit benzin & elektrik' or i['fuel_type'] == 'hibrit dizel & elektrik':
                if 'electric_set' in i:
                    i['electric_set']['package'] = package.id
                    serializer_electric_set = ElectricSerializer(data=i['electric_set'])
                    if not serializer_electric_set.is_valid():
                        model.package_set.all().delete()
                        model.euroncap_set.all().delete()
                        return Response(serializer_electric_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_electric_set.save(package=package)
                if 'hybrid_set' in i:
                    i['hybrid_set']['package'] = package.id
                    serializer_hybrid_set = HybridSerializer(data=i['hybrid_set'])
                    if not serializer_hybrid_set.is_valid():
                        model.package_set.all().delete()
                        model.euroncap_set.all().delete()
                        return Response(serializer_hybrid_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_hybrid_set.save(package=package)

            i['fuel_set']['package'] = package.id
            serializer_fuel_set = FuelSerializer(data=i['fuel_set'])
            if not serializer_fuel_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_fuel_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_fuel_set.save(package=package)

            i['control_set']['package'] = package.id
            serializer_control_set = ControlSerializer(data=i['control_set'])
            if not serializer_control_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_control_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_control_set.save(package=package)

            i['transmission_set']['package'] = package.id
            serializer_transmission_set = TransmissionSerializer(data=i['transmission_set'])
            if not serializer_transmission_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_transmission_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_transmission_set.save(package=package)

            i['suspension_set']['package'] = package.id
            serializer_suspension_set = SuspensionSerializer(data=i['suspension_set'])
            if not serializer_suspension_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_suspension_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_suspension_set.save(package=package)

            i['dimension_set']['package'] = package.id
            serializer_dimension_set = DimensionSerializer(data=i['dimension_set'])
            if not serializer_dimension_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_dimension_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_dimension_set.save(package=package)

            i['offroad_set']['package'] = package.id
            serializer_offroad_set = OffRoadSerializer(data=i['offroad_set'])
            if not serializer_offroad_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_offroad_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_offroad_set.save(package=package)

            i['still_set']['package'] = package.id
            serializer_still_set = StillSerializer(data=i['still_set'])
            if not serializer_still_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_still_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_still_set.save(package=package)

            i['comfort_set']['package'] = package.id
            serializer_comfort_set = ComfortSerializer(data=i['comfort_set'])
            if not serializer_comfort_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_comfort_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_comfort_set.save(package=package)

            i['security_set']['package'] = package.id
            serializer_security_set = SecuritySerializer(data=i['security_set'])
            if not serializer_security_set.is_valid():
                model.package_set.all().delete()
                model.euroncap_set.all().delete()

                return Response(serializer_security_set.errors, status=status.HTTP_400_BAD_REQUEST)
            serializer_security_set.save(package=package)

            if 'paket_set' in i:
                for j in i['paket_set'].values():
                    j['package'] = package.id
                    # send_message_to_user(
                    #     "445951537",
                    #     f'Создана модель {j}'
                    # )
                    serializer_paket_set = PaketSerializer1(data=j)
                    if not serializer_paket_set.is_valid():
                        model.package_set.all().delete()
                        model.euroncap_set.all().delete()
                        return Response(serializer_paket_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_paket_set.save(package=package)
            if 'option_set' in i:
                for j in i['option_set'].values():
                    j['package'] = package.id
                    serializer_option_set = OptionSerializer(data=j)
                    if not serializer_option_set.is_valid():
                        model.package_set.all().delete()
                        model.euroncap_set.all().delete()

                        return Response(serializer_option_set.errors, status=status.HTTP_400_BAD_REQUEST)
                    serializer_option_set.save(package=package)
            send_message_to_user(
                "445951537",
                f'{request.user.username}\n'
                f'{datetime.now()}\n'
                f'Редактирован пакет {i["name"]}'
                f'{i}'
            )
        send_message_to_user(
            "445951537",
            f'{request.user.username}\n'
            f'{datetime.now()}\n'
            f'Редактирована модель {data["model"]["name"]}\n'
        )
        return Response({"message": "Успешно создано"}, status=status.HTTP_201_CREATED)
