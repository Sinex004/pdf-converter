from django.db.models import Sum, Avg
from django.shortcuts import render
from django.http import Http404
from datetime import datetime
from django.forms.models import model_to_dict
from django.views.generic.edit import CreateView
from django import forms
from django.http import HttpResponseRedirect
from django.urls import reverse, reverse_lazy

from users.converters import img_to_pdf, doc_to_pdf_linux, pdf_to_doc_linux
from users.models import ConvertedFile


class ConvertedFileForm(forms.Form):
    upload = forms.FileField(
        label='Select a file',
        help_text='max. 42 megabytes'
    )


def list_view(request):
    # Handle file upload
    if request.method == 'POST':
        form = ConvertedFileForm(request.POST, request.FILES)
        if form.is_valid():
            newdoc = ConvertedFile(upload=request.FILES['upload'])
            newdoc.save()

            # Redirect to the document list after POST
            return HttpResponseRedirect(reverse(list_view))
    else:
        form = ConvertedFileForm()  # A empty, unbound form

    # Load documents for the list page
    documents = ConvertedFile.objects.all()

    # Render list page with the documents and the form
    return render(
        request,
        'list.html',
        {'documents': documents, 'form': form},
    )


class ImgToPdfView(CreateView):
    model = ConvertedFile
    fields = ['upload', ]
    success_url = reverse_lazy('img_to_pdf')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['documents'] = ConvertedFile.objects.filter(type='Img_To_Pdf')
        return context

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        response = super().form_valid(form)
        self.object.type = 'Img_To_Pdf'
        self.object.download = img_to_pdf(form.instance)
        self.object.save()
        return response


class DocToPdfView(CreateView):
    model = ConvertedFile
    fields = ['upload', ]
    success_url = reverse_lazy('doc_to_pdf')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['documents'] = ConvertedFile.objects.filter(type='Doc_To_Pdf')
        return context

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        response = super().form_valid(form)
        self.object.type = 'Doc_To_Pdf'
        self.object.download = doc_to_pdf_linux(form.instance)
        self.object.save()
        return response


class PDFToDocView(CreateView):
    model = ConvertedFile
    fields = ['upload', ]
    success_url = reverse_lazy('pdf_to_doc')

    def get_context_data(self, **kwargs):
        context = super().get_context_data(**kwargs)
        context['documents'] = ConvertedFile.objects.filter(type='Pdf_To_Doc')
        return context

    def form_valid(self, form):
        # This method is called when valid form data has been POSTed.
        # It should return an HttpResponse.
        response = super().form_valid(form)
        self.object.type = 'Pdf_To_Doc'
        self.object.download = pdf_to_doc_linux(form.instance)
        self.object.save()
        return response
