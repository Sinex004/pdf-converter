from django.contrib import admin
from users.models import ConvertedFile
from django.contrib.auth.models import Group


class ConvertedFileAdmin(admin.ModelAdmin):
    model = ConvertedFile


admin.site.register(ConvertedFile, ConvertedFileAdmin)
admin.site.unregister(Group)

# Настройки Хедеров в Админке
admin.site.site_header = 'Панель администратора'
admin.site.site_title = 'Админ-панель PDF Converter'
admin.site.index_title = 'PDF Converter Admin'

