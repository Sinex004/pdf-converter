import img2pdf
from PIL import Image
import subprocess
HOME_DIRECTORY = '/home/pdf-converter/media/'
import aspose.words as aw


def path_img_to_pdf(converted_file):
    array_name = converted_file.upload.name.split(".")
    array_name[-1] = '_img_to_pdf.pdf'
    return ''.join(array_name)


def path_doc_to_pdf(converted_file):
    array_name = converted_file.upload.name.split(".")
    array_name[-1] = '_doc_to_pdf.pdf'
    return ''.join(array_name)


def path_pdf_to_doc(converted_file):
    array_name = converted_file.upload.name.split(".")
    array_name[-1] = '_pdf_to_doc.docx'
    return ''.join(array_name)


def img_to_pdf(converted_file):
    # opening image
    image = Image.open(converted_file.upload.path)
    # converting into chunks using img2pdf
    pdf_bytes = img2pdf.convert(image.filename)
    pdf_path = path_img_to_pdf(converted_file)
    # opening or creating pdf file
    file = open(f"media/{pdf_path}", "wb")
    # writing pdf files with chunks
    file.write(pdf_bytes)
    # closing image file
    image.close()
    # closing pdf file
    file.close()
    return pdf_path


def doc_to_pdf_linux(converted_file):
    # doc_result = path_doc_to_pdf(converted_file)
    doc_result = f'result doc_to_pdf.pdf'
    # Load the PDF document from the disc.
    doc = aw.Document(converted_file.upload.path)
    # Save the document to DOCX format.
    doc.save(doc_result)
    return doc_result


def pdf_to_doc_linux(converted_file):
    # doc_result = path_pdf_to_doc(converted_file)
    doc_result = f'result pdf_to_doc.docx'
    # Load the PDF document from the disc.
    doc = aw.Document(converted_file.upload.path)
    # Save the document to DOCX format.
    doc.save(doc_result)
    return doc_result


# def doc_to_pdf_linux(converted_file):
#     doc_result = path_doc_to_pdf(converted_file)
#     doc = converted_file.upload.name
#     cmd = 'libreoffice --convert-to pdf'.split() + [f'{doc}']
#     p = subprocess.Popen(cmd, stderr=subprocess.PIPE, stdout=subprocess.PIPE)
#     p.wait(timeout=30)
#     stdout, stderr = p.communicate()
#     if stderr:
#         raise subprocess.SubprocessError(stderr)
#     return doc_result