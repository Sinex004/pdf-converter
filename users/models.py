from django.conf import settings
from django.core.files.storage import FileSystemStorage
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager, PermissionsMixin

# fs = FileSystemStorage(location=settings.STATIC_ROOT)


def upload_directory_path(instance, filename):
    return f'{filename}'


def download_directory_path(instance, filename):
    return f'{filename}_converted'


class ConvertedFile(models.Model):
    TYPE_CHOICES = [
        ('Doc_To_Pdf', 'Doc_To_Pdf'),
        ('Img_To_Pdf', 'Img_To_Pdf'),
        ('Pdf_To_Img', 'Pdf_To_Img'),
        ('Pdf_To_Doc', 'Pdf_To_Doc'),
    ]
    type = models.CharField(
        max_length=256,
        verbose_name='Тип конвертации',
        choices=TYPE_CHOICES,
        default='Активно'
    )
    upload = models.FileField(
        upload_to=upload_directory_path,
    )
    download = models.FileField(
        upload_to=download_directory_path,
        null=True,
        blank=True
    )
    created_at = models.DateTimeField(auto_now_add=True)

    class Meta:
        verbose_name = 'Конвертированный файлы'
        verbose_name_plural = 'Конвертированные файлы'

    def __str__(self):
        return f'{self.id}'
