"""project URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.conf.urls.static import static
from django.conf import settings
from django.contrib import admin
from rest_framework.authtoken.views import obtain_auth_token
from django.urls import path, include
from users.views import (
    list_view, ImgToPdfView, DocToPdfView, PDFToDocView
)
from vacancies.views import (
    models_list, brands_list, brand_create, model_create, model_delete, model_edit, model_view, model_gallery,
    config_header, city_models_list, brand_models_list, model_detail, salons_list, brand_salons_list,
    model_gallery_edit, salon_detail, register_view, comment_detail, rating_detail, comments_list, comment_dislike,
    comment_like, model_detail_copy
)

urlpatterns = [
    path('admin/', admin.site.urls),
    path('auth/', obtain_auth_token, name='api_token_auth'),  # получение токена
    path('register/', register_view, name='register_view'),

    # path('', home_view, name='home'),  # Главная страница
    path('list/', list_view, name='list'),
    path('brands/', brands_list, name='brands_list'),
    path('brands/new/', brand_create, name='brand_create'),
    path('brands/<int:brand_id>/models/', models_list, name='models_list'),
    path('brands/<int:brand_id>/models/new/', model_create, name='model_create'),
    path('brands/<int:brand_id>/models/<int:model_id>/gallery/', model_gallery, name='model_gallery'),
    path('brands/<int:brand_id>/models/<int:model_id>/delete/', model_delete, name='model_delete'),
    path('brands/<int:brand_id>/models/<int:model_id>/edit/', model_edit, name='model_edit'),
    path('brands/<int:brand_id>/models/<int:model_id>/gallery/edit/', model_gallery_edit, name='model_gallery_edit'),

    path('brands/<int:brand_id>/models/<int:model_id>/', model_view, name='model_view'),


    path('config', config_header, name='config_header'),
    path('cars', city_models_list, name='city_models_list'),
    path('cars/', city_models_list, name='city_models_list'),
    path('cars/<slug:brand_name>', brand_models_list, name='brand_models_list'),
    path('cars/<slug:brand_name>/', brand_models_list, name='brand_models_list'),

    path('cars/<slug:brand_name>/<slug:model_name>/comment', comments_list, name='comments_list'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/', comments_list, name='comments_list/'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/create', comment_detail, name='comment_detail'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/create/', comment_detail, name='comment_detail/'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/<int:comment_id>/like', comment_like, name='comment_like'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/<int:comment_id>/like/', comment_like, name='comment_like/'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/<int:comment_id>/dislike', comment_dislike, name='comment_dislike'),
    path('cars/<slug:brand_name>/<slug:model_name>/comment/<int:comment_id>/dislike/', comment_dislike, name='comment_dislike/'),
    path('cars/<slug:brand_name>/<slug:model_name>/rating', rating_detail, name='rating_detail'),
    path('cars/<slug:brand_name>/<slug:model_name>/rating/', rating_detail, name='rating_detail/'),

    path('cars/<slug:brand_name>/<slug:model_name>/<slug:type_tab>', model_detail, name='model_detail'),
    path('cars/<slug:brand_name>/<slug:model_name>/<slug:type_tab>/', model_detail, name='model_detail/'),
    path('cars/<slug:brand_name>/<slug:model_name>/<slug:type_tab>/<int:model_package>/<slug:package_name>', model_detail_copy, name='model_detail_copy'),
    path('cars/<slug:brand_name>/<slug:model_name>/<slug:type_tab>/<int:model_package>/<slug:package_name>/', model_detail_copy, name='model_detail_copy/'),
    path('sellers/cars', salons_list, name='salons_list'),
    path('sellers/cars/', salons_list, name='salons_list/'),
    path('sellers/cars/<slug:brand_name>', brand_salons_list, name='brand_salons_list'),
    path('sellers/cars/<slug:brand_name>/', brand_salons_list, name='brand_salons_list/'),
    path('sellers/cars/<slug:brand_name>/<int:salon_id>', salon_detail, name='salon_detail'),
    path('sellers/cars/<slug:brand_name>/<int:salon_id>/', salon_detail, name='salon_detail/'),

    # path('cars/<slug:brand_name>', brand_models_list, name='brand_models_list'),
    # path('cars/<slug:brand_name>', brand_models_list, name='brand_models_list'),

    path('convert/img_to_pdf/', ImgToPdfView.as_view(), name='img_to_pdf'),
    path('convert/doc_to_pdf/', DocToPdfView.as_view(), name='doc_to_pdf'),
    path('convert/pdf_to_doc/', PDFToDocView.as_view(), name='pdf_to_doc'),

] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
